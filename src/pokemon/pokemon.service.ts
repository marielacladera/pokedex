import { Injectable } from '@nestjs/common';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { Model } from 'mongoose';
import { Pokemon } from './entities/pokemon.entity';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class PokemonService {

  constructor(
    @InjectModel(Pokemon.name)
    private readonly _pokemonModel: Model<Pokemon>
  ) {

  }

  public async create (createPokemonDto: CreatePokemonDto) {
    createPokemonDto.name = createPokemonDto.name.toLocaleLowerCase();
    const pokemon = await this._pokemonModel.create(createPokemonDto);
    return pokemon;
  }

  public findAll() {
    return `This action returns all pokemon`;
  }

  public findOne(id: number) {
    return `This action returns a #${id} pokemon`;
  }

  public update(id: number, updatePokemonDto: UpdatePokemonDto) {
    return `This action updates a #${id} pokemon`;
  }

  public remove(id: number) {
    return `This action removes a #${id} pokemon`;
  }
}

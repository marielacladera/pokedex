import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PokemonService } from './pokemon.service';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';

@Controller('pokemon')
export class PokemonController {

  constructor(
    private readonly _pokemonService: PokemonService
  ) {}

  @Post()
  public create(@Body() createPokemonDto: CreatePokemonDto) {
    return this._pokemonService.create(createPokemonDto);
  }

  @Get()
  public findAll() {
    return this._pokemonService.findAll();
  }

  @Get(':id')
  public findOne(@Param('id') id: string) {
    return this._pokemonService.findOne(+id);
  }

  @Patch(':id')
  public update(@Param('id') id: string, @Body() updatePokemonDto: UpdatePokemonDto) {
    return this._pokemonService.update(+id, updatePokemonDto);
  }

  @Delete(':id')
  public remove(@Param('id') id: string) {
    return this._pokemonService.remove(+id);
  }
}
